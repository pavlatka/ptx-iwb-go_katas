package main

import (
  "fmt"
  "strconv"
)

func CreatePhoneNumber(numbers [10]uint) string {

  s := "("
  for i, v := range numbers {
    s = s + strconv.FormatUint(uint64(v), 10)

    switch i {
      case 2: s = s + ") "
      case 5: s = s + "-"
    }
  }

  return s
}

func main () {
  fmt.Println(CreatePhoneNumber([10]uint{1,2,3,4,5,6,7,8,9,0}))
}
