package main

import (
  "fmt"
)

func printerError(s string) string {
  b := 0
  for _, v := range []rune(s) {
    if v < 97 || v > 109 {
      b++
    }
  }

  return fmt.Sprintf("%d/%d", b, len(s))
}

func main () {
  fmt.Println(printerError("aaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz"))
}
