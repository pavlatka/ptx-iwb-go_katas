package main

import (
  "fmt"
)

func findStart(maze [][]string) (int, int) {
  for r, rv := range maze {
    for c, cv := range rv {
      if cv == "s" {
        return r, c
      }
    }
  }

  panic("Could not find the start point")
}

func main() {
  fmt.Println("MAZE")

  maze := [][]string {
    {"*", "*", "*", "s", "*", "*", "*", "*"},
    {"*", " ", " ", " ", "w"," ", " ","*"},
    {"*", " ", " ", " ", "w"," ", " ","*"},
    {"*", " ", " ", " ", "w"," ", " ","e"},
    {"*", " ", " ", " ", " "," ", " ","*"},
    {"*", " ", " ", " ", " "," ", " ","*"},
    {"*", "*", "*", "*", "*", "*", "*", "*"},
  }

  for _, r := range maze {
    for _, c := range r {
      fmt.Print(c)
    }

    fmt.Println()
  }

  fmt.Println(findStart(maze))

}
