package main

import (
  "fmt"
)

func revrot(s string, n int) string {
  if n <= 0 || n > len(s) {
    return ""
  }

  ch := s[0:n]

  fmt.Println(ch)

  return ""
}

func main() {
  fmt.Println(revrot("123456987654", 6))
}
